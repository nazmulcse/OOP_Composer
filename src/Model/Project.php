<?php
/**
 * Created by PhpStorm.
 * User: Nazmul Hasan
 * Date: 24-Jun-15
 * Time: 6:23 PM
 */

namespace App\Model;


class Project {
    public $title='';
    public $status='';
    public function __construct($title="Default Title"){
        $this->title=$title;
        $this->status="Pending";
    }
} 